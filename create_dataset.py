import sys, getopt, os, pytube, cv2, itertools
import sqlite3

from sqlite3 import Error
from os import listdir
from os.path import isfile, join

dataset_string = ""
n_frame = 0
path = ""

options = "hd:f:p:"
long_options = ["help", "dataset_string=", "n_frame=", "path="]
frames_path = os.getcwd() + "/frames/"
database_path = os.getcwd() + "/database.db"

def create_connection(db_file):
	"""
	create a database connection to the SQLite database specified by db_file

		parameters db_file (str): database file

		returns conn (obj): connection object or None
	"""
	connection = None
	try:
		connection = sqlite3.connect(db_file)
		return connection
	except Error as e:
		print(e)
	return connection

def get_all_search_string(connection):
	"""
	get a list of all search_string exist in videos table

		parameters:
			connection (obj): connection object

		returns:
			list (str): list of all search_string
	"""
	cur = connection.cursor()
	cur.execute("SELECT DISTINCT search_string FROM videos")
	rows = cur.fetchall()	
	return list(itertools.chain(*rows))

def get_matching_dataset(search_string_list, dataset_string):
	"""
	sort search_string list, keep search_string match with dataset_string

		parameters:
			search_string_list (list(str)): all search_string from database
			dataset_string (str): keyword want to search

		returns:
			result (list(str)): list of matching dataset_string
	"""
	result = []
	for search_string in search_string_list:
		if dataset_string in search_string:
			result.append(search_string)
	return result

def get_video_by_dataset(connection, dataset_list):
	"""
	get all video_id by matching dataset_string

		parameters:
			connection (obj): connection object
			dataset_list (str): list of matching dataset_string

		returns:
			result (list(str)): list of video_id
	"""
	result = []
	cur = connection.cursor()
	for dataset in dataset_list:
		cur.execute("SELECT video_id FROM videos WHERE search_string = '" + dataset + "'")
		rows = cur.fetchall()
		result = result + list(itertools.chain(*rows))
	return result

def find_frames(connection, dataset_string, n_frame):
	"""
	get n_frame of each video_id

		parameters:
			connection (obj): connection object
			dataset_string (str): keyword want to search
			n_frame (int): number of frame to create dataset

		returns:
			result (list(str)): list of frame_id
	"""
	result = []

	search_string_list = get_all_search_string(connection)
	dataset_list = get_matching_dataset(search_string_list, dataset_string)
	if (not dataset_list):
		print("dataset_string not found")
		sys.exit()
	video_list = get_video_by_dataset(connection, dataset_list)

	cur = connection.cursor()
	for video in video_list:
		cur.execute("SELECT frame_id FROM frames WHERE video_id = '" + video + "'")
		rows = cur.fetchall()
		result = result + list(itertools.chain(*rows))[:n_frame]
	return result

def create_dataset(frames_list, path):
	"""
	create dataset named by path

		parameters:
			frames_list (list(str)): list of frame_id
			path (str): directory of dataset to store
	"""
	try:
		if not os.path.exists(path):
			os.makedirs(path)
	except OSError:
		print("Error: Can not create dataset directory")
	for frame in frames_list:
		file = frames_path + frame + ".jpg"
		try:
			os.system("cp " + file + " " + path)
		except OSError:
			print("Error: Can not copy file to " + path)

def main(argv):
	try:
		arguments, values = getopt.getopt(argv, options, long_options)
	except getopt.GetoptError:
		display_help()
	for argument, value in arguments:
		if argument in ("-h", "--help"):
			display_help()
		elif argument in ("-d", "--dataset_string"):
			global dataset_string
			dataset_string = value
		elif argument in ("-f", "--n_frame"):
			global n_frame
			n_frame = int(value)
		elif argument in ("-p", "--path"):
			global path
			path = os.getcwd() + "/" + value

if __name__ == "__main__":

	#run main
	main(sys.argv[1:])

	#create connection to database
	conn = create_connection(database_path)

	#create tables
	if conn is not None:
		pass
	else:
		print("Error: Can not create database connection")

	frames = find_frames(conn, dataset_string, n_frame)
	create_dataset(frames, path)