# VideoToFrame

## Content

- Create a program to download video from youtube and extract frames from video
- All frames store in a local database using sqlite (in database.db at root of project)

## Installation

- Install Python: https://www.python.org/downloads/
- Install pip: https://pip.pypa.io/en/stable/installing/

### Requirements

- Install pytube:
	```
	pip install pytube
	pip3 install pytube
	```
- Install youtube search python:
	```
	pip install youtube-search-python
	pip3 install youtube-search-python
	```
- Install OpenCV:
	```
	pip install opencv-python
	pip3 install opencv-python
	```
- Install sqlite:
	```
	pip install sqlite
	pip3 install sqlite
	```

### Run
	```
	python download_video.py --search_string <search content> --n_video <number of video to take> --n_frame <number of frame to extract>

	python create_dataset.py --dataset_string <dataset content> --n_frame <number of frame to create dataset --path <dataset directory name>
	```

## Features

- Search video on youtube and download n_video from the beginning
- Extract frame from video, storage into a directory
- Delete all video after extract frames
- Storage into a database
- Create dataset