import sys, getopt, os, pytube, cv2
import sqlite3

from sqlite3 import Error
from youtubesearchpython import VideosSearch
from os import listdir
from os.path import isfile, join

options = "hs:v:f:"
long_options = ["help", "search_string=", "n_video=", "n_frame="]
videos_path = os.getcwd() + "/videos/"
frames_path = os.getcwd() + "/frames/"
database_path = os.getcwd() + "/database.db"

create_videos_table_query = """CREATE TABLE IF NOT EXISTS videos (
							video_id text NOT NULL PRIMARY KEY,
							video_url text NOT NULL,
							video_title text NOT NULL,
							search_string text NOT NULL
					);"""

create_frames_table_query = """CREATE TABLE IF NOT EXISTS frames (
							frame_id text PRIMARY KEY,
							video_id text NOT NULL,
							timecode text NOT NULL,
							path_to_frame text NOT NULL,
							FOREIGN KEY (video_id) REFERENCES videos (video_id)
					);"""

search_string = ""
n_video = 0
n_frame = 0

def create_connection(db_file):
	"""
	create a database connection to the SQLite database specified by db_file

		parameters db_file (str): database file

		returns conn (obj): connection object or None
	"""
	connection = None
	try:
		connection = sqlite3.connect(db_file)
		return connection
	except Error as e:
		print(e)
	return connection

def create_table(connection, create_table_query):
	"""
	create a table from the create_table_query
		parameters:
			connection (obj): Connection object
			create_table_query (str): a CREATE TABLE query
	"""
	try:
		c = connection.cursor()
		c.execute(create_table_query)
	except Error as e:
		print(e)

def insert_video(connection, video):
	"""
	insert information into videos table
		parameters:
			connection (obj): Connection object
			video (dict): information of video
	"""
	video_item = (video["video_id"], video["video_url"], video["video_title"], video["search_string"])
	query = """INSERT INTO videos(video_id, video_url, video_title, search_string) VALUES(?, ?, ?, ?)"""
	cur = connection.cursor()
	cur.execute(query, video_item)
	connection.commit()
	return cur.lastrowid

def insert_frame(connection, frame):
	"""
	insert information into frames table
		parameters:
			connection (obj): Connection object
			frame (dict): information of frame
	"""
	frame_item = (frame["frame_id"], frame["video_id"], str(frame["timecode"]), frame["path_to_frame"])
	query = """INSERT INTO frames(frame_id, video_id, timecode, path_to_frame) VALUES(?, ?, ?, ?)"""
	cur = connection.cursor()
	cur.execute(query, frame_item)
	connection.commit()
	return cur.lastrowid

def display_help():
	print("python download_video.py --search_string <keyword> --n_video <number of video to download> --n_frame <number of frame>")
	sys.exit()

def video_search(keyword, n_video):
	"""
	return list of videos after searching on youtube

		parameters:
			keyword (str): search string, what videos to search
			n_video (int): number of video count from the beginning to take

		returns:
			videos (list(dict)): list of videos from searching, each video store as a dict
	"""
	videos = []
	video_list = VideosSearch(keyword, limit = n_video)

	for video in video_list.result()["result"]:
		video = {
			"video_id": video["id"],
			"video_url": video["link"],
			"video_title": video["title"],
			"search_string": keyword
		}
		videos.append(video)

	return videos

def download_videos(connection, videos_list):
	"""
	download all videos in video_list and store in videos_path

		paramenters:
			videos_list (list(dict)): list of videos from searching, each video store as a dict
	"""
	for element in videos_list:
		youtube = pytube.YouTube(element["video_url"])
		video = youtube.streams.get_highest_resolution()
		video.download(videos_path, filename = element["video_id"])

def create_path_to_frame(video, timecode):
	"""
	create direct path to the frame in video on youtube

		parameters:
			video (dict): dictionary of video (content: video_id, video_url, video_title, search_string)
			timecode (double): timestamp of the frame in ms

		returns:
			path (str): path to the frame in video 
	"""
	#transform millisecod into x(h)y(m)z(s) format
	seconds = timecode//1000
	timecode = timecode%1000
	minutes = 0
	hours = 0

	if seconds >= 60:
		minutes = seconds//60
		seconds = seconds % 60

	if minutes >= 60:
		hours = minutes//60
		minutes = minutes % 60

	#add time point of frame into youtube path
	return video["video_url"] + "#t=" + str(hours) + "h" + str(minutes) + "m" + str(seconds) + "s"

def extract_frames(videos_list, n_frame):
	"""
	extract all frames from videos list:

		parameters:
			videos_list (list(dict)): list of videos from searching, each video store as a dict
			n_frame (int): number of frame to extract from each video

		returns:
			frames_list (list(dict)): list of frame for all video
	"""
	frames_list = []

	try:
		if not os.path.exists(frames_path):
			os.makedirs(frames_path)
	except OSError:
		print("Error: Can not create frame directory")

	for video in videos_list:
		camera_capture = cv2.VideoCapture(videos_path + video["video_id"] + ".mp4")
		current_frame = 0
		while(current_frame < n_frame):
			read_success, frame = camera_capture.read()
			milliseconds = camera_capture.get(cv2.CAP_PROP_POS_MSEC)
			path_to_frame = create_path_to_frame(video, milliseconds)
			if read_success:
				name = frames_path + video["video_id"] + "_frame" + str(current_frame) + ".jpg"
				print("Creating..." + name)
				cv2.imwrite(name, frame)
				frame_save = {
					"frame_id": (video["video_id"] + "_frame" + str(current_frame)),
					"video_id": video["video_id"],
					"timecode": milliseconds,
					"path_to_frame": path_to_frame
				}
				frames_list.append(frame_save)
				current_frame += 1
			else:
				break
	camera_capture.release()
	cv2.destroyAllWindows()

	#after extracting frames, remove video directory
	try:
		if os.path.exists(videos_path):
			os.system("rm -rf " + videos_path)
	except OSError:
		print("Error: Can not remove directory")

	return frames_list

def main(argv):
	try:
		arguments, values = getopt.getopt(argv, options, long_options)
		#print(values)
	except getopt.GetoptError:
		display_help()
	for argument, value in arguments:
		if argument in ("-h", "--help"):
			display_help()
		elif argument in ("-s", "--search_string"):
			global search_string
			search_string = value
		elif argument in ("-v", "--n_video"):
			global n_video
			n_video = int(value)
		elif argument in ("-f", "--n_frame"):
			global n_frame
			n_frame = int(value)


if __name__ == "__main__":

	#run main
	main(sys.argv[1:])

	#create connection to database
	conn = create_connection(database_path)

	#create tables
	if conn is not None:
		create_table(conn, create_videos_table_query)
		create_table(conn, create_frames_table_query)
	else:
		print("Error: Can not create database connection")

	#search video
	videos = video_search(search_string, n_video)

	#download video
	download_videos(conn, videos)

	#insert video information into videos table
	for video in videos:
		insert_video(conn, video)

	#extract frames from video
	frames = extract_frames(videos, n_frame)

	#insert frame into frames table
	for frame in frames:
		insert_frame(conn, frame)